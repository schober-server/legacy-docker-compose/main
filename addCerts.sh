#!/bin/bash

: "${WEBROOT:=/var/www/acme-challenge}"

echo "Webroot is '${WEBROOT}'"

docker run  -it --rm --name certbot \
            -v schoberserver_letsencrypt:/etc/letsencrypt \
            -v schoberserver_acme-challenge:"${WEBROOT}" \
            certbot/certbot \
            certonly \
            -d schober.space \
            -d cloud.schober.space \
            -d frp.schober.space \
            -d hass.schober.space \
            -d onlyoffice.schober.space \
            -d photos.schober.space \
            -d webdav.schober.space \
            -d www.schober.space \
            -w "${WEBROOT}"
            

docker exec schoberserver_nginx_1 nginx -s reload

