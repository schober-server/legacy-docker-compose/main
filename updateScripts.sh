#!/bin/env bash
# To replace all scripts in home dir:
# ROOT_DIR=$HOME SRC_SCRIPTS="$HOME/main/restart.sh $HOME/main/update.sh" ~/main/updateScripts.sh

: "${ROOT_DIR:=$(dirname "$0")}"
: "${SRC_SCRIPTS:=\"$ROOT_DIR/restart.sh\" \"$ROOT_DIR/update.sh\"}"

echo "ROOT_DIR=$ROOT_DIR"
echo "SRC_SCRIPTS=$SRC_SCRIPTS"

eval "parent_scripts=($SRC_SCRIPTS)"
declare -a children_scripts

for script in ${parent_scripts[@]}; do
    script_name_w_exension="${script##*/}"
    script_name="${script_name_w_exension%.*}"
    var_name="scripts_$script_name"

    echo "Saving $script in $var_name"
    scripts="$(find "$ROOT_DIR" -type f -iname "$script_name_w_exension")"
    scripts=${scripts/$script/} # Remove parent script from children scripts

    eval $var_name=\$scripts
    children_scripts+=("$var_name")
done

echo "Found the following scripts to replace:"

for i in ${!parent_scripts[@]}; do
  echo "[$i] ${parent_scripts[$i]} to replace ${children_scripts[$i]}:"

  for child_script in ${!children_scripts[$i]}; do
    printf "\t%s\n" "$child_script"
  done
done


if [[ "$(
    read -r -e -p 'Continue? [y/N]> '
    echo "$REPLY"
)" != [Yy]* ]]; then
    echo "Exiting..."
    exit 1
fi


for i in ${!parent_scripts[@]}; do
    for child_script in ${!children_scripts[$i]}; do
        cat "${parent_scripts[$i]}" > "$child_script"
    done
done

echo "Done!"
