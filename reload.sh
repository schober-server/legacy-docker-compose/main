#!/bin/sh

path=$(dirname $(realpath $0))
cd $path
echo $path

docker-compose exec nginx nginx -s reload
